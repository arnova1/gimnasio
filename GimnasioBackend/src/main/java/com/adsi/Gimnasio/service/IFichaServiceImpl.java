package com.adsi.Gimnasio.service;

import com.adsi.Gimnasio.domain.Ficha;
import com.adsi.Gimnasio.repository.FichaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class IFichaServiceImpl implements IFichaService{

    @Autowired
    FichaRepository fichaRepository;

    @Override
    public Iterable<Ficha> read() {
        return fichaRepository.findAll();
    }

    @Override
    public Ficha create(Ficha ficha) {
        return fichaRepository.save(ficha);
    }

    @Override
    public Ficha update(Ficha ficha) {
        return fichaRepository.save(ficha);
    }

    @Override
    public void delete(Integer idFicha) {
        fichaRepository.deleteById(idFicha);
    }

    @Override
    public Optional<Ficha> getByIdFicha(Integer idFicha) {
        return fichaRepository.findById(idFicha);
    }
}
