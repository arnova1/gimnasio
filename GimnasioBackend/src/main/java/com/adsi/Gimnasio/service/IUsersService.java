package com.adsi.Gimnasio.service;

import com.adsi.Gimnasio.domain.Users;

import java.util.Optional;
// frima de los metodos
public interface IUsersService {

    //metodo para leer
    public Iterable<Users> read();

    //metodo para crear
    public Users create(Users users);

    //metodo para actualizar
    public Users update(Users users);

    //metodo para borrar
    public void delete(Integer idUsers);

    //busqueda por ID
    public Optional<Users> getByIdFicha(Integer idUsers);


}
