package com.adsi.Gimnasio.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Users {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(columnDefinition = "serial")
    private Integer idUsers;

    @Column(length = 20)
    private String name;

    @Column(length = 20)
    private String lastName;

    @Column(length = 15)
    private String documentNumber;

    @Column(length = 25)
    private String email;

    @Column(length = 15)
    private String phone;

    @ManyToOne
    private Rols rols;

    @ManyToOne
    private Ficha dicha;
}
