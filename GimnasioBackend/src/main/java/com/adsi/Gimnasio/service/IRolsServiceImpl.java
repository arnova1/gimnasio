package com.adsi.Gimnasio.service;

import com.adsi.Gimnasio.domain.Rols;
import com.adsi.Gimnasio.repository.RolsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class IRolsServiceImpl implements IRolsService{

    @Autowired
    RolsRepository rolsRepository;

    @Override
    public Iterable<Rols> read() {
        return rolsRepository.findAll();
    }

    @Override
    public Rols create(Rols rols) {
        return rolsRepository.save(rols);
    }

    @Override
    public Rols update(Rols rols) {
        return rolsRepository.save(rols);
    }

    @Override
    public void delete(Integer idRols) {
        rolsRepository.deleteById(idRols);
    }

    @Override
    public Optional<Rols> getByIdRols(Integer idRols) {
        return rolsRepository.findById(idRols);
    }
}
